
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';


export class BaseService {

  api_url;
  http;

  constructor(plural: string, sentHttp: HttpClient) {
    this.http = sentHttp;
    this.api_url = environment.apiUrl + '/' + plural;
  }


  getList(): Observable<any[]> {

    return this.http
      .get(this.api_url)
      .map(response => {
        return response['data'] as any[];
      })
      .catch(this.handleError);
  }

  getSingle(id: string): Observable<any> {
    return this.http
      .get(this.api_url + '/' + id)
      .map(response => {
        return response['data'] as any;
      })
      .catch(this.handleError);
  }


  create(model: any): Observable<any> {
    return this.http
      .post(this.api_url, model)
      .map(response => {
        return response['data'] as any;
      })
      .catch(this.handleError);
  }


  update(model: any): Observable<any> {
    return this.http
      .put(this.api_url + '/' + model._id, model)
      .map(response => {
        return model.__v = response['__v'];
      })
      .catch(this.handleError);
  }

  delete(id: any): Observable<null> {
    return this.http
      .delete(this.api_url + '/' + id)
      .map(response => null)
      .catch(this.handleError);
  }

  handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

}
