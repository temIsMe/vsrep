import { Component, OnInit } from '@angular/core';
import {Player} from '../player.model';
import {PlayerService} from '../player.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-player-details',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.scss']
})
export class PlayerDetailsComponent implements OnInit {

  player: Player;

  constructor(private service: PlayerService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.player = this.route.snapshot.data['entity'];
  }

}
