var mongoose = require('mongoose');

var Team = new mongoose.Schema({
  name: String,
  country: String

});

module.exports = mongoose.model('Team', Team);
