var express = require('express');
var router = express.Router();
var BaseController = require('./baseController');
var mongoModel = require('../models/team.model');

class TeamController extends BaseController {


  /**
   @param model Mongoose model
   @param key primary key of the model that will be used for searching, removing
   and reading
   */
  constructor() {
    super(mongoModel,router);
  }


}

module.exports = TeamController;
