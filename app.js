var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


const DIST_FOLDER = 'dist';
/** avoid Error: Password contains an illegal unescaped character*/
function getConnection() {
  return process.env.CUSTOMCONNSTR_atlas.replace('{0}', encodeURIComponent(process.env.atlas_pass));
}

var mongoose = require('mongoose');
var db =
  process.env.CUSTOMCONNSTR_atlas ?
  getConnection() :
  'mongodb://bigchiff:{0}@cluster0-shard-00-00-byfa1.mongodb.net:27017,cluster0-shard-00-01-byfa1.mongodb.net:27017,cluster0-shard-00-02-byfa1.mongodb.net:27017/vfDb?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin'
  .replace('{0}', encodeURIComponent('Bigchiff#1'))
// 'mongodb://127.0.0.1:27017/vfDb'
;

mongoose
  .connect(
    db, {
      promiseLibrary: require('bluebird'),
      useNewUrlParser: true,
    }
  )
  .then(() => {
    console.log(`Succesfully Connected to the Mongodb Database at URL : ` + db);
  })
  .catch(e => {
    console.log(`Error Connecting to  : ` + db, e);
  });

var app = express();

/**  view engine setup */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(favicon(path.join(__dirname, DIST_FOLDER, 'favicon.ico')));
app.use(logger('tiny'));

// body-parser extract the entire body portion of an incoming request stream and exposes it on req.body
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(cookieParser());

/** Set routes*/
var api = require('./server/routes/api.route');
app.use('/api', api);


app.use('/.well-known', express.static('.well-known')); // for ssl
/** User Angular files from dist folder*/
app.use(express.static(path.join(__dirname, DIST_FOLDER)));
app.use('*', express.static(path.join(__dirname, DIST_FOLDER)));


/** catch 404 and forward to error handler */
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(req);
});

// keep the 'next' anyways
app.use(async (err, req, res, next) => {

  const message = res.message || err.message || res.statusMessage || 'no error message';

  res.locals.message = message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log('ERROR', err.stack);

  const ans = err.message ? {
    message: message,
    stack: err.stack,
  } : {
    err
  };

  return res
    .status(err.status || 500)
    .json(ans);

});
module.exports = app;
