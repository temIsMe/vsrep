import {Component, OnInit} from '@angular/core';
import {TeamService} from '../team.service';
import {Team} from '../team.model';

import {ListService} from '../../../shared/list.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit {

  list: Team[] = [];
  selectedTeam: Team = new Team();

  displayedColumns = ['name', 'country', 'delete'];

  constructor(private service: TeamService,
              private listService: ListService) {
  }

  ngOnInit() {
    this.service.getList()
      .subscribe(items => this.list = items);
  }

  editMe(team: Team) {
    this.selectedTeam = team;
  }

  deleteMe(team: Team) {
    this.service.delete(team._id).subscribe(res => {
      this.list = this.listService.removeItem(this.list, team._id);
    });
  }

  addTeam(team: Team) {
    this.list = [...this.list, team];
  }
}
