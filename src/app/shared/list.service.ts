import {Injectable} from '@angular/core';

@Injectable()
export class ListService {

  constructor() {
  }

  removeItem(list, id: any) {
    const foundIndex = list.findIndex(x => x.id === id);
    if (foundIndex !== -1) {
      list.splice(foundIndex, 1);
    }
    return list;

    // return array.filter(e => e !== element);
  }

  moveItem(src, dest, id: any) {
    const foundIndex = src.findIndex(x => x.id === id);
    const item = src.splice(foundIndex, 1);
    dest.push(item);
  }


}
