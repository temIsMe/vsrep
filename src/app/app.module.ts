import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {MatchListComponent} from './match/match-list/match-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatTableModule, MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import {MatchEditComponent} from './match/match-edit/match-edit.component';
import {MatchResolver} from './match/match-resolver.service';
import {MatchService} from './match/match.service';
import {FormsModule} from '@angular/forms';
import {MatchEventListComponent} from './match-event/match-event-list/match-event-list.component';
import {CommonModule} from '@angular/common';
import {MatchEventEditComponent} from './match-event/match-event-edit/match-event-edit.component';
import {ListService} from './shared/list.service';
import {HttpClientModule} from '@angular/common/http';
import {AdminModule} from './admin/admin.module';
import {GameMinPipe} from './shared/pipes/game-min.pipe';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import { MatchStatsComponent } from './match/match-stats/match-stats.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { ChanceByTypeComponent } from './match/match-stats/chance-by-type/chance-by-type.component';
import { ChanceByOrderComponent } from './match/match-stats/chance-by-order/chance-by-order.component';
import { ChanceByTimeComponent } from './match/match-stats/chance-by-time/chance-by-time.component';

@NgModule({
  declarations: [
    AppComponent,
    MatchListComponent,
    MatchEditComponent,
    MatchEventListComponent,
    MatchEventEditComponent,
    GameMinPipe,
    MatchStatsComponent,
    ChanceByTypeComponent,
    ChanceByOrderComponent,
    ChanceByTimeComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatInputModule,
    MatSliderModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    MatGridListModule,
    MatListModule,
    MatIconModule,
    MatTabsModule,
    AdminModule,
    AppRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxChartsModule,
  ],
  exports: [],
  providers: [MatchResolver, MatchService, ListService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
