import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {BaseService} from '../shared/base.service';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class MatchService extends BaseService {

  constructor( http: HttpClient) {
    super('matches', http);
  }

  nextId(list) {
    if (!list.length) {
      return '1';
    }
    const id = parseInt(list[list.length - 1].id, 0) + 1;
    return id.toString();
  }


  getCompetitions() {
    return ['ליגת העל', 'גביע המדינה', 'הליגה הספרדית'];
  }

}

