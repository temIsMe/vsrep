import {Pipe, PipeTransform} from '@angular/core';
import {Match} from '../../match/match.model';

@Pipe({
  name: 'minute'
})
export class GameMinPipe implements PipeTransform {
  /** get action minute*/
  transform(min: string, match: Match): string {

    const diff = (+new Date(min)) - (+new Date(match.date));
    let minFromStart = Math.ceil((diff / 1000) / 60);
    if (minFromStart <= 45) {
      return minFromStart.toString();
    }

    /** first half overtime*/
    if (minFromStart < 60) {
      return '45+' + (minFromStart - 45);
    }

    /** remove break, and scnd half delay*/
    minFromStart -= 15;
    minFromStart -= match.scndDelay || 0;

    if (minFromStart <= 90) {
      return minFromStart.toString();
    }

    /** second half overtime*/

    return '90+' + (minFromStart - 90);

  }

}
