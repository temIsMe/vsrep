import {Component, Input, OnInit} from '@angular/core';
import * as _ from 'underscore';
import {ChanceType} from '../stats.const';
import {Match, MatchEvent} from '../../match.model';


@Component({
  selector: 'app-chance-by-type',
  templateUrl: './chance-by-type.component.html',
  styleUrls: ['./chance-by-type.component.scss']
})
export class ChanceByTypeComponent implements OnInit {

  @Input('match') match: Match;
  @Input('events') events: MatchEvent[];
  @Input('colorScheme') colorScheme: string[];
  multi;

  constructor() {

  }

  onSelect(event) {

  }

  ngOnInit() {
    this.setData(this.events);

  }

  private setData(events: MatchEvent[]) {

    const lm = 0.3;
    const mh = 0.7;

    this.multi = Object.entries(_.groupBy(events, 'team')).map(g => {
      const id = g[0];
      const teamSide = this.match.home._id === id ? 'home' : 'away';
      const eventsGroups = _.countBy(g[1], function (el) {
        return el.eXg > mh ? ChanceType.high :
          ( el.eXg > lm ? ChanceType.medium : ChanceType.low);
      });

      return {
        name: this.match[teamSide].name,
        series: [
          this.typeCnt(eventsGroups, ChanceType.high),
          this.typeCnt(eventsGroups, ChanceType.medium),
          this.typeCnt(eventsGroups, ChanceType.low)
        ]
      };
    });

    //console.log(this.multi);
  }

  private typeCnt(eventsGroups: _.Dictionary<number>, key: string) {
    return {
      'name': key,
      'value': eventsGroups[key] || 0
    };
  }

}
