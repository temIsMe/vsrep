import {MatchListComponent} from './match-list/match-list.component';
import {MatchEditComponent} from './match-edit/match-edit.component';
import {MatchResolver} from './match-resolver.service';
import {MatchStatsComponent} from './match-stats/match-stats.component';

export const MatchRoute = {
  path: 'matches',
  children: [
    {
      path: '',
      component: MatchListComponent
    },
    {
      path: ':id',
      resolve: {
        entity: MatchResolver
      },
      children: [
        {
          path: '',
          component: MatchEditComponent
        },
        {
          path: 'stats',
          component: MatchStatsComponent
          // resolve: {
          //   entity: MatchResolver
          // }
        }
      ]
    }
  ]
};
