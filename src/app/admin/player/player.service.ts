import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {Player} from './player.model';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class PlayerService {
  api_url = environment.apiUrl + '/players';

  constructor(private http: HttpClient) {
  }

  getSingle(matchId: string): Observable<Player> {
    return this.http
      .get(this.api_url + '/' + matchId)
      .map(response => {
        return response['data'] as Player;
      })
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}
