import { Injectable } from '@angular/core';
import {PlayerService} from './player.service';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';

@Injectable()
export class PlayerResolverService implements Resolve<any> {

  constructor(private service: PlayerService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.getSingle(route.params['id']);
  }

}
