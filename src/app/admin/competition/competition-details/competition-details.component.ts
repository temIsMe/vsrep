import {Component, OnInit} from '@angular/core';
import {Competition} from '../competition.model';
import {CompetitionService} from '../competition.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Team} from '../../team/team.model';
import {TeamService} from '../../team/team.service';
import {CompetitionTeamService} from '../../competition-team.service';


@Component({
  selector: 'app-competition-details',
  templateUrl: './competition-details.component.html',
  styleUrls: ['./competition-details.component.scss']
})
export class CompetitionDetailsComponent implements OnInit {

  competition: Competition;
  teams: Team[];
  newTeamId;

  constructor(private service: CompetitionService,
              private teamService: TeamService,
              private competitionTeamService: CompetitionTeamService,
              private _location: Location,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.competition = this.route.snapshot.data['entity'];
    this.teamService.getDrop().subscribe(res => {
      this.teams = res;
    });
  }

  saveMe() {
    this.service[this.competition._id ? 'update' : 'create'](this.competition).subscribe(res => {
      // if (!this.competition._id) {
      //   //this.teamAdded.emit(res);
      // }
      this._location.back();
    });

  }


  addCompTeam() {
    this.competitionTeamService.create(this.newTeamId, this.competition._id).subscribe(res => {
      this.competition.teams = [...this.competition.teams, this.teams.find(x => x._id === this.newTeamId)];
      this.newTeamId = null;
    });
    // this.competition.teams.push(newTeam);
    // this.service.update(this.competition).subscribe(res => {
    //
    // });
  }

  deleteCompTeam(team: any) {
    this.competitionTeamService.delete(team._id, this.competition._id).subscribe(res => {
      this.competition.teams = this.competition.teams.filter(e => e !== team);
    });
  }
}
