import {Pipe, PipeTransform} from '@angular/core';
import * as _ from 'underscore';

@Pipe({
  name: 'excludeItems'
})
export class ExcludeItemsPipe implements PipeTransform {

  transform(value: any[], toRemove?: any[]): any {
    return _.reject(value, function (item) {
      return _.findWhere(toRemove, {'_id': item['_id']});
    });
  }

}
