import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Match, MatchEvent} from '../../match/match.model';

@Component({
  selector: 'app-match-event-list',
  templateUrl: './match-event-list.component.html',
  styleUrls: ['./match-event-list.component.scss']
})
export class MatchEventListComponent implements OnInit {
  @Input('events') events: MatchEvent[];
  @Input('selectedEvent') selectedEvent: MatchEvent;
  @Input('match') match: Match;

  @Output()
  eventSelected = new EventEmitter();
  @Output()
  eventDeleted = new EventEmitter();

  displayedColumns = ['time', 'team', 'playerName', 'result', 'eventType', 'delete'];

  constructor() {
  }

  ngOnInit() {
  }

  editMe(event: MatchEvent) {
    this.eventSelected.emit(event);
    // this.selectedEvent = event;
  }

  deleteMe(event: MatchEvent) {
    this.eventDeleted.emit(event);
  }


}
