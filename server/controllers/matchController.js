var express = require('express');
var router = express.Router();
var BaseController = require('./baseController');
var MatchService = require('../services/match.service');
var mongoModel = require('../models/match.model');

class MatchController extends BaseController {


  /**
   @param model Mongoose model
   @param key primary key of the model that will be used for searching, removing
   and reading
   */
  constructor() {
    super(mongoModel, router);
  }

  async read(id) {
    return await MatchService.getSinglePopulated(this.model, id)
      .then(this.result);
  }
}

module.exports = MatchController;
