import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {CompetitionService} from './competition.service';

@Injectable()
export class CompetitionResolverService  implements Resolve<any> {

  constructor(private service: CompetitionService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.getSingle(route.params['id']);
  }

}
