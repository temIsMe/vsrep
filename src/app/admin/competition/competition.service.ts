import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../../shared/base.service';
import {Competition} from './competition.model';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CompetitionService extends BaseService {

  constructor(http: HttpClient) {
    super('competitions', http);
  }

  getDrop(): Observable<Competition[]> {
    return this.http
      .get(this.api_url, {
        params: {
          func: 'drop'
        }
      })
      .map(response => {
        return response['data'] as Competition[];
      })
      .catch(this.handleError);
  }
}
