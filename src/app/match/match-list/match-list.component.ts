import {Component, OnInit} from '@angular/core';
import {Match} from '../match.model';
import {MatchService} from '../match.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.scss']
})
export class MatchListComponent implements OnInit {

  matches: Match[];

  constructor(private service: MatchService,
              private router: Router) {
  }

  ngOnInit() {
    this.service.getList()
      .subscribe(items => this.matches = items);

    // this.matches = this.service.get();
  }


  token() {
    function rand() {
      return Math.random().toString(36).substr(2); // remove `0.`
    }

    return rand() + rand(); // to make it longer
  }

  addMatch() {
    // const newMatch: Match = new Match();
    // this.service.create(newMatch).subscribe(res => {
    //     this.router.navigate(['matches', res._id]);
    //   }, (err) => {
    //     console.log(err);
    //   }
    // );
    this.router.navigate(['matches', '0']);
  }
}
