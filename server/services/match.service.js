
exports.getMany = async function (model) {
  try {
    return await model
      .find({})
      .populate('competition', 'name')
      .sort({'date': -1})
      .populate('home', 'name')
      .populate('away', 'name')
      .limit(50);
  } catch (e) {
    throw e;
  }
};

exports.getSinglePopulated = async function (model, id) {
  try {
    return await model.findById(id)
      .populate('home', 'name')
      .populate('away', 'name')
      ;
  } catch (e) {
    throw e;
  }
};
