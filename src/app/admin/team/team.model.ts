export class Team {
  _id?: string;
  __v?: number;
  name: string;
  country: string;
}
