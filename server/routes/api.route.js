var matchController = require("../controllers/matchController") ;
var playerController = require("../controllers/playerController");
var competitionController = require("../controllers/competitionController");
var teamController = require("../controllers/teamController");
var competitionTeamController = require("../controllers/competitionTeamController");

var express = require('express');
var router = express.Router();

router.use('/matches', new matchController().routes());
router.use('/competitions', new competitionController().routes());
router.use('/players', new playerController().routes());
router.use('/teams', new teamController().routes());
router.use('/competitionTeams', new competitionTeamController().routes());

module.exports = router;
