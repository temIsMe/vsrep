var EntityService = require('../services/base.service');

class BaseController {


  /**
   @param model Mongoose model
   @param key primary key of the model that will be used for searching, removing
   and reading
   */
  constructor(model,routerSrc, key ="_id") {
    this.router = routerSrc;
    this.model = model;
    this.modelName = model.modelName.toLowerCase();

    this.key = key;
  }

  result(data) {
    return {data};
  }

  async drop(){
    return this.list();
  }

  async list() {
    return await EntityService.getMany(this.model)
      .then(this.result);
  }

  async read(id) {
    return await EntityService.getSingle(this.model, id)
      .then(this.result);
  }

  async create(data) {
    return await EntityService.create(this.model, data)
      .then(this.result);
  }

  async update(id, data) {
    return await EntityService.update(this.model, id, data)
      .then((data) => {
        return {__v: data.__v};
      });
  }

  async delete(id) {
    return await EntityService.delete(this.model, id)
      .then(() => {
        return {};
      });
  }

  routes() {
    var router =  this.router;
    /**
     Returns a function that will write the result as a JSON to the response
     */
    function ok(res) {
      return (data) => {
        res.json(data);
      };
    }

    /**
     Depending on the error type, will perform the following:
     Object was not found - 404 Not Found
     Invalid or missing input parameter - 400 Bad request
     Not enough privileges - 401 Unauthorized
     Unknown error - 500 Internal server error
     */
    function fail(res) {
      return (error) => {
        console.log("Error " + error._message, error);
        res.sendStatus(404).end();
      };
    };


    router.get("/", (req, res) => {

      this[req.query.func || 'list']()
        .then(ok(res))
        .then(null, fail(res));
    });

    router.post("/", (req, res) => {
      this
        .create(req.body)
        .then(ok(res))
        .then(null, fail(res));
    });

    router.get("/:key", (req, res) => {
      this
        .read(req.params.key)
        .then(ok(res))
        .then(null, fail(res));
    });

    router.put("/:key", (req, res) => {
      this
        .update(req.params.key, req.body)
        .then(ok(res))
        .then(null, fail(res));
    });

    router.delete("/:key/:childKey", (req, res) => {

      this
        .delete(req.params.key, req.params.childKey)
        .then(ok(res))
        .then(null, fail(res));
    });

    return router;
  }
}

module.exports = BaseController;
