import {TeamListComponent} from './team-list/team-list.component';
import {TeamDetailsComponent} from './team-details/team-details.component';
import {TeamResolverService} from './team-resolver.service';

export const  TeamRoute = {
  path: 'teams',
  children: [
    {
      path: '',
      component: TeamListComponent
    },
    {
      path: ':id',
      component: TeamDetailsComponent,
      resolve: {
        entity: TeamResolverService
      }
    }
  ]
};
