import {PlayerListComponent} from './player-list/player-list.component';
import {PlayerDetailsComponent} from './player-details/player-details.component';
import {PlayerResolverService} from './player-resolver.service';

export const PlayerRoute = {
  path: 'players',
  children: [
    {
      path: '',
      component: PlayerListComponent
    },
    {
      path: ':id',
      component: PlayerDetailsComponent,
      resolve: {
        entity: PlayerResolverService
      }
    }
  ]
};
