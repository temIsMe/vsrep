var express = require('express');
var router = express.Router();
var BaseController = require('./baseController');
var CompetitionService = require('../services/competition.service');
var mongoModel = require('../models/competition.model');

class CompetitionController extends BaseController {


  /**
   @param model Mongoose model
   @param key primary key of the model that will be used for searching, removing
   and reading
   */
  constructor() {
    super(mongoModel,router);
  }

  /** get only name*/
  async list() {
    return await CompetitionService.getIndex(this.model)
      .then(this.result);
  }

  async drop() {
    return await CompetitionService.getDrop(this.model)
      .then(this.result);
  }

  async read(id) {
    return await CompetitionService.getSinglePopulated(this.model, id)
      .then(this.result);
  }

}

module.exports = CompetitionController;
