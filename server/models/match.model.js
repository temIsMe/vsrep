var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Match = new mongoose.Schema({
  // id: String,
  competition: {type: Schema.ObjectId, ref: 'Competition', required: true},
  date: Date,
  scndDelay: Number,
  home: {type: Schema.ObjectId, ref: 'Team', required: true},
  away: {type: Schema.ObjectId, ref: 'Team', required: true},
  goalsH: Number,
  goalsA: Number,
  eXgH: Number,
  eXgA: Number,
  events: []
});

module.exports = mongoose.model('Match', Match);
