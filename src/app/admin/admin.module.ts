import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CompetitionListComponent} from './competition/competition-list/competition-list.component';
import {CompetitionDetailsComponent} from './competition/competition-details/competition-details.component';
import {PlayerService} from './player/player.service';
import {PlayerListComponent} from './player/player-list/player-list.component';
import {PlayerDetailsComponent} from './player/player-details/player-details.component';
import {AdminRoutingModule} from './admin-routing.module';
import {PlayerResolverService} from './player/player-resolver.service';
import {CompetitionResolverService} from './competition/competition-resolver.service';
import {CompetitionService} from './competition/competition.service';
import {
  MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule,
  MatTableModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {TeamService} from './team/team.service';
import {TeamResolverService} from './team/team-resolver.service';
import {TeamListComponent} from './team/team-list/team-list.component';
import {TeamDetailsComponent} from './team/team-details/team-details.component';
import {ListService} from '../shared/list.service';
import {CompetitionTeamService} from './competition-team.service';
import {ExcludeItemsPipe} from '../shared/pipes/exclude-items.pipe';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AdminRoutingModule,
    MatFormFieldModule,
    MatTableModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
  ],
  declarations: [CompetitionListComponent, CompetitionDetailsComponent, PlayerListComponent, PlayerDetailsComponent, TeamListComponent, TeamDetailsComponent, ExcludeItemsPipe],
  providers: [ListService, CompetitionService, CompetitionResolverService, PlayerService, PlayerResolverService, TeamService, TeamResolverService, CompetitionTeamService]
})
export class AdminModule {
}
