import {Component, Input, OnInit} from '@angular/core';
import {Match, MatchEvent} from '../../match.model';
import * as _ from 'underscore';

@Component({
  selector: 'app-chance-by-time',
  templateUrl: './chance-by-time.component.html',
  styleUrls: ['./chance-by-time.component.scss']
})
export class ChanceByTimeComponent implements OnInit {
  @Input('match') match: Match;
  @Input('events') events: MatchEvent[];
  @Input('colorScheme') colorScheme: string[];
  single;

  constructor() {
  }

  ngOnInit() {
    this.setData(this.events);

  }

  private setData(events: MatchEvent[]) {

    const lm = 0.3;
    const mh = 0.7;

    const ans = events.map(x => {
      const teamSide = this.match.home._id === x.team.toString() ? 'home' : 'away';
      return {
        'name': x.time,
        'value': x.eXg * (this.match.home._id === x.team.toString() ? -1 : 1)
      };
    });
    this.single = ans;
    /*
        this.single = Object.entries(_.groupBy(events, 'team')).map(g => {
          const id = g[0];
          const teamSide = this.match.home._id === id ? 'home' : 'away';
          const eventsGroups = _.countBy(g[1], function (el) {
            return el.eXg > mh ? ChanceType.high :
              ( el.eXg > lm ? ChanceType.medium : ChanceType.low);
          });

          return {
            name: this.match[teamSide].name,
            series: [
              this.typeCnt(eventsGroups, ChanceType.high),
              this.typeCnt(eventsGroups, ChanceType.medium),
              this.typeCnt(eventsGroups, ChanceType.low)
            ]
          };
        });
    */
    // console.log(this.single);
  }

  private typeCnt(eventsGroups: _.Dictionary<number>, key: string) {
    return {
      'name': key,
      'value': eventsGroups[key] || 0
    };
  }

}
