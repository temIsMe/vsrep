import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Match, MatchEvent} from '../../match/match.model';
import {GameMinPipe} from '../../shared/pipes/game-min.pipe';
import {CHANCES} from '../../shared/chancesTable';

@Component({
  selector: 'app-match-event-edit',
  templateUrl: './match-event-edit.component.html',
  styleUrls: ['./match-event-edit.component.scss'],
  providers: [GameMinPipe]
})
export class MatchEventEditComponent implements OnChanges, OnInit {

  esTable;
  chances;
  eXgTypes: string[];
  mint;
  @Input('event') event: MatchEvent;
  @Input('match') match: Match;
  // newEvent = {id: 'cc', time: new Date(), playerName: 'טוני', eventType: 'מצב הבקעה'};
  types = ['נסיון הבקעה', 'חילוף', 'כרטיס'];

  @Output()
  saveEvent = new EventEmitter();
  @Output()
  addEvent = new EventEmitter();
  this;

  constructor(private pipe: GameMinPipe) {
    this.chances  = CHANCES;
    this.esTable = 'kick';
    this.eXgTypes = Object.keys(CHANCES);
  }

  ngOnChanges(changes: SimpleChanges) {

    this.timeToMin();
  }

  ngOnInit() {
  }

  addMe() {
    this.addEvent.emit(this.event);
  }

  saveMe() {
    if (this.event.eXg) {
      this.event.eXg = Math.round(this.event.eXg * 100) / 100;
    }
    if (this.event.id) {
      return this.saveEvent.emit();
    }
    this.addMe();

  }

  /** reset extra info if type was changed*/
  typeChanged() {
    this.event.eXg = this.event.isGoal = this.event.cardColor = null;
  }

  teamChanged() {
    this.event.time = new Date();
    this.timeToMin();
  }

  minToTime() {
    if (!this.event.time) {
      return;
    }

    const arr = this.mint.split('+');
    let min = parseInt(arr[0], 0);
    if (min > 45) {
      min += 15;
    }
    if (arr.length > 1) {
      min += parseInt(arr[1], 0);
    }


    const d = new Date(this.match.date);
    d.setMinutes(d.getMinutes() + (min + (this.match.scndDelay || 0)));
    this.event.time = d;
  }

  timeToMin() {
    if (!this.event.time) {
      return;
    }
    this.mint = this.pipe.transform(this.event.time.toString(), this.match);
  }

  delayChanged() {
    this.saveEvent.emit();
    this.minToTime();
  }
}
