import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CompetitionRoute} from './competition/competition.route';
import {PlayerRoute} from './player/player.route';
import {TeamRoute} from './team/team.route';


const routes: Routes = [
  CompetitionRoute,
  PlayerRoute,
  TeamRoute
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
