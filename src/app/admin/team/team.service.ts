import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {BaseService} from '../../shared/base.service';
import {Team} from './team.model';

@Injectable()
export class TeamService extends BaseService {

  constructor(http: HttpClient) {
    super('teams', http);
  }

  getDrop() {
    return this.http.get(this.api_url)
      .map(response => {
        return response['data'] as Team[];
      })
      .catch(this.handleError);
  }
}
