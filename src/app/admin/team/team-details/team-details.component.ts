import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Team} from '../team.model';
import {TeamService} from '../team.service';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent implements OnInit {
  @Input('team')team: Team;

  @Output()
  teamAdded = new EventEmitter();


  constructor(private service: TeamService) {
  }

  ngOnInit() {
  }

  saveMe() {
    this.service[this.team._id ? 'update' : 'create'](this.team).subscribe(res => {
      if (!this.team._id) {
        this.teamAdded.emit(res);
      }
      this.team = new Team();
    });
  }

}
