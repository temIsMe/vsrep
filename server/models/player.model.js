var mongoose = require('mongoose');

var Player = new mongoose.Schema({
  name: String
});

module.exports = mongoose.model('Player', Player);
