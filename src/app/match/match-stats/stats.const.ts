export enum ChanceType {
  high = 'גבוה',
  medium = 'בינוני',
  low = 'נמוך'
}
