import {Team} from '../team/team.model';

export class Competition {
  _id?: string;
  __v?: number;
  name: string;
  year: string;
  country: string;
  teams: any[];
}
