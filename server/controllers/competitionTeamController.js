var express = require('express');
var router = express.Router();
var BaseController = require('./baseController');
var EntityService = require('../services/competitionTeam.service');
var mongoModel = require('../models/competition.model');

class CompetitionTeamController extends BaseController {

  constructor() {
    super(mongoModel, router);
  }

  async create(params) {

    if (!params.parentId || !params.id) {
      throw "params missing";
    }
    return await EntityService.create(this.model, params.parentId, params.id)
      .then(this.result);
  }


  async delete(parentId, id) {
    return await EntityService.delete(this.model, parentId, id)
      .then(() => {
        return {};
      });
  }

}

module.exports = CompetitionTeamController;
