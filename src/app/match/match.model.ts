import {Competition} from '../admin/competition/competition.model';
import {Team} from '../admin/team/team.model';

export class Match {

  _id?: string;
  __v?: number;
  competition: Competition;
  date?: Date;
  scndDelay?: number;
  home: Team;
  away: Team;
  goalsH: number;
  goalsA: number;
  eXgH: number;
  eXgA: number;
  events?: MatchEvent[];

  constructor() {
    this.goalsH = this.goalsA = this.eXgH = this.eXgA = 0;
    this.date = new Date();
    this.home = new Team();
    this.away = new Team();
    this.events = [];
  }
}

export class MatchEvent {
  id: string;
  time: Date;
  team: Team;
  playerName: string;
  eventType: string;
  eXg?: number;
  isGoal?: boolean;
  cardColor?: boolean;

  constructor() {
    this.eventType = 'נסיון הבקעה';
  }
}
