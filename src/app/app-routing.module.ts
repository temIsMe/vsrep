import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MatchRoute} from './match/match.route';

const routes: Routes = [
  {path: '', redirectTo: '/matches', pathMatch: 'full'},
  MatchRoute
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
