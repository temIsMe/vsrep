var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MatchEvent = new mongoose.Schema({
  time: Date,
  team: {type: Schema.ObjectId, ref: 'Team', required: true},
  playerName: {type: Schema.ObjectId, ref: 'Player'},
  eventType: {
    type: String,
    enum: ['נסיון הבקעה', 'חילוף', 'כרטיס'],
    required: true
  },
  eXg: Number,
  isGoal: Boolean,
  cardColor: Boolean
});

module.exports = mongoose.model('MatchEvent', MatchEvent);
