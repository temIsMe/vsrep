import {CompetitionListComponent} from './competition-list/competition-list.component';
import {CompetitionDetailsComponent} from './competition-details/competition-details.component';
import {CompetitionResolverService} from './competition-resolver.service';

export const  CompetitionRoute = {
  path: 'competitions',
  children: [
    {
      path: '',
      component: CompetitionListComponent
    },
    {
      path: ':id',
      component: CompetitionDetailsComponent,
      resolve: {
        entity: CompetitionResolverService
      }
    }
  ]
};
