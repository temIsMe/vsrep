import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Match, MatchEvent} from '../match.model';
import {MatchService} from '../match.service';
import {CompetitionService} from '../../admin/competition/competition.service';
import {Competition} from '../../admin/competition/competition.model';
import {Team} from '../../admin/team/team.model';
import * as _ from 'underscore';

@Component({
  selector: 'app-match-edit',
  templateUrl: './match-edit.component.html',
  styleUrls: ['./match-edit.component.scss']
})
export class MatchEditComponent implements OnInit {

  match: Match = new Match();
  selectedEvent: MatchEvent = new MatchEvent();
  competitions: Competition[] = [];
  teams: Team[];

  constructor(private service: MatchService,
              private competitionService: CompetitionService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.match = this.route.snapshot.data['entity'];

    this.competitionService.getDrop().subscribe(res => {
      this.competitions = res;
      this.competitionChange(false);

    });
  }


  onChange() {
    if (this.match.home._id && this.match.away._id) {
      this.saveMatch();
    }
  }

  addMatchEvent() {
    this.selectedEvent.id = this.service.nextId(this.match.events);
    this.match.events = [this.selectedEvent, ...this.match.events];

    this.saveMatchEvent();
  }

  deleteMatchEvent(event: MatchEvent) {
    this.match.events = this.match.events.filter(e => e !== event);
    this.updateScore();
    this.service.update(this.match).subscribe();

  }

  saveMatch() {
    return this.match._id ? this.update() : this.add();
  }

  competitionChange(save = true) {
    if (this.match.competition) {
      this.teams = this.competitions.find(x => x._id === this.match.competition.toString()).teams;
      if (save) {
        this.onChange();
      }
    }

  }

  saveMatchEvent() {
    this.updateScore();
    this.saveMatch();
  }

  onTeamChange(team: Team, val) {
    team.name = val.source.selected.viewValue;
  }

  private updateScore() {
    this.setTeamStats(this.match.home, 'H');
    this.setTeamStats(this.match.away, 'A');
  }

  private setTeamStats(team: Team, side: string) {
    const events = this.match.events.filter(event => event.team.toString() === team._id && event.eventType === 'נסיון הבקעה');
    this.match['goals' + side] = _.filter(events, function (event) {
      return event.isGoal;
    }).length;

    const sum =  _.reduce(events.map(x => x.eXg), function (memo, num) {
      return memo + num;
    }, 0) || 0;
    /** fix problem with many digits after period. leave only 2*/
    this.match['eXg' + side] = Math.round(sum * 100) / 100;

  }

  private update() {
    this.service.update(this.match).subscribe(res => {
      this.selectedEvent = new MatchEvent();
    });
  }

  private add() {
    this.service.create(this.match).subscribe(res => {
      this.match._id = res._id;
      this.router.navigate(['matches', res._id]);
    });
  }
}
