import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {MatchService} from './match.service';
import {Match} from './match.model';

@Injectable()
export class MatchResolver implements Resolve<any> {

  constructor(private service: MatchService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params['id'];
    return id != 0 ? this.service.getSingle(route.params['id']) : new Match();
  }

}

