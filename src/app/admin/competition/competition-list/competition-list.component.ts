import { Component, OnInit } from '@angular/core';
import {CompetitionService} from '../competition.service';
import {Competition} from '../competition.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-competition-list',
  templateUrl: './competition-list.component.html',
  styleUrls: ['./competition-list.component.scss']
})
export class CompetitionListComponent implements OnInit {

  list: Competition[] = [];

  constructor(private service: CompetitionService,
              private router: Router) {
  }

  ngOnInit() {
    this.service.getList()
      .subscribe(items => this.list = items );
  }

  addCompetition() {
    const newCompetition: Competition = new Competition();
    this.service.create(newCompetition).subscribe(res => {
        this.router.navigate(['competitions', res._id]);
      }, (err) => {
        console.log(err);
      }
    );
  }
}
