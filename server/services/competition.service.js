exports.getIndex = async function (model) {
  try {
    return await model
      .find({})
      .select('name');
  } catch (e) {
    throw e;
  }
};

exports.getSinglePopulated = async function (model, id) {
  try {
    return await model.findById(id)
      .populate('teams','name')
      ;
  } catch (e) {
    throw e;
  }
};

exports.getDrop = async function (model) {
  try {
    return await model
      .find({})
      .select('name teams')
      .populate('teams','name')

  } catch (e) {
    throw e;
  }
};
