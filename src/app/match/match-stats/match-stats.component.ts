import {Component, OnInit} from '@angular/core';
import {MatchService} from '../match.service';
import {Match, MatchEvent} from '../match.model';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'underscore';


@Component({
  selector: 'app-match-stats',
  templateUrl: './match-stats.component.html',
  styleUrls: ['./match-stats.component.scss']
})
export class MatchStatsComponent implements OnInit {
  match: Match;
  events: MatchEvent[];
  colorScheme = {
    domain: ['#2e7d32', '#f9a825', '#c62828']
  };


  constructor(private service: MatchService,
              private route: ActivatedRoute) {
    this.match = this.route.snapshot.data['entity'];
    this.events = this.match.events.filter(event => event.eventType === 'נסיון הבקעה');

  }

  ngOnInit() {
  }


}


