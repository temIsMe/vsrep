import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {TeamService} from './team.service';

@Injectable()
export class TeamResolverService implements Resolve<any> {

  constructor(private service: TeamService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.getSingle(route.params['id']);
  }

}

