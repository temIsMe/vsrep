import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseChildService} from '../shared/base-child.service';

@Injectable()
export class CompetitionTeamService extends BaseChildService {

  constructor(http: HttpClient) {
    super('competitionTeams', http);
  }

}
