
exports.create = async function (model, parentId, id) {
  try {
    return await model.findById(parentId)
      .then((competition) => {
        competition.teams.push(id);
        return competition.save();
      });
  } catch (e) {
    throw e;
  }

};

exports.delete = async function (model, competitionId, teamId) {
  try {
    return await model.findById(competitionId)
      .then((competition) => {
        competition.teams = competition.teams.filter(e => e != teamId);
        return competition.save();
      });
  } catch (e) {
    throw e;
  }
};
