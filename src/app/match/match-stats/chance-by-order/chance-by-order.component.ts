import {Component, Input, OnInit} from '@angular/core';
import {Match, MatchEvent} from '../../match.model';
import {ChanceType} from '../stats.const';
import * as _ from 'underscore';

const lm = 0.3;
const mh = 0.7;

@Component({
  selector: 'app-chance-by-order',
  templateUrl: './chance-by-order.component.html',
  styleUrls: ['./chance-by-order.component.scss']
})
export class ChanceByOrderComponent implements OnInit {

  @Input('match') match: Match;
  @Input('events') events: MatchEvent[];
  @Input('colorScheme') colorScheme: string[];
  multi;


  constructor() {
  }

  ngOnInit() {
    this.setData(this.events);

  }

  private setData(events: MatchEvent[]) {


    this.multi = Object.entries(_.groupBy(events, 'team')).map(g => {
      const teamId = g[0];
      const teamSide = this.match.home._id === teamId ? 'home' : 'away';
      const teamChances = g[1];
      const series = [
        this.empty(ChanceType.high),
        this.empty(ChanceType.medium),
        this.empty(ChanceType.low)
      ];

      teamChances.forEach(chance => {
        series.push({
          'name': this.getType(chance.eXg),
          'value': chance.eXg
        });
      });

      return {
        name: this.match[teamSide].name,
        series: series
      };
    });


  }

  private getType(eXg) {
    return eXg > mh ? ChanceType.high :
      ( eXg > lm ? ChanceType.medium : ChanceType.low);
  }

  private empty(key) {
    return {
      name: key,
      value: 0
    };
  }

}


export const MULTI = [
  {
    'name': 'Germany',
    'series': [
      {
        'name': '2010',
        'value': 7300000
      },
      {
        'name': '2011',
        'value': 8940000
      },
      {
        'name': '2010',
        'value': 702000
      }
    ]
  },

  {
    'name': 'USA',
    'series': [
      {
        'name': '2010',
        'value': 7870000
      },
      {
        'name': '2011',
        'value': 8270000
      }
    ]
  },
];
