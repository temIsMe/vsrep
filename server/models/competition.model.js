var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Competition = new mongoose.Schema({
  name: String,
  year: String,
  country: String,
  teams: [  {
    type: Schema.ObjectId,
    ref: "Team"
  }]
});

Competition
  .virtual('title')
  .get(function () {
    return this.name + ' ' + this.year;
  });


module.exports = mongoose.model('Competition', Competition);
